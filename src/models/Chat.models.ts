import { model, Schema, Document } from 'mongoose'

export interface IChat extends Document {
    userID: string
    name: string
    msg: string
    date: string
}
const chatSchema = new Schema<IChat>(
    {
        userID: String,
        name: String,
        msg: String,
        date: String
    },
    {
        timestamps: true,
        versionKey: false,
    }
)

export default model<IChat>('Chat', chatSchema)