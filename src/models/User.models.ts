import { model, Schema, Document } from 'mongoose'
import { genSalt, hash, compare } from 'bcrypt'

export interface IUser extends Document {
    name: string
    lastname: string
    email: string
    password: string
    comparePassword: (password: string) => Promise<boolean>
}

const userSchema = new Schema<IUser>(
    {
        name: {
            type: String,
            required: true
        },
        lastname: {
            type: String
        },
        email: {
            type: String,
            unique: true,
            required: true,
            lowercase: true,
            trim: true,
        },
        password: {
            type: String,
            required: true,
        },
    },
    {
        timestamps: true,
        versionKey: false,
    }
)


userSchema.pre<IUser>('save', async function (next) {
    const self = this
    const user = self
    if (!user.isModified('password')) return next()
    const salt = await genSalt(10)
    const cryptHash = await hash(user.password, salt)
    user.password = cryptHash
    next()
})

userSchema.methods.comparePassword = async function (password: string): Promise<boolean> {
    return await compare(password, this.password)
}
export default model<IUser>('User', userSchema)