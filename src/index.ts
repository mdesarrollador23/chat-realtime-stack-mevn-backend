import server from './app'
import './database'

server.listen(process.env.PORT || 4000)
console.log(`Servidor escuchando: http://localhost:${process.env.PORT || 4000}`)