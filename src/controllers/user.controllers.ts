import { Request, Response } from 'express'
import User, { IUser } from '../models/User.models'
import {sign} from 'jsonwebtoken'


class AuthController {
    //Registra Usuarios
    public static signUp = async (req: Request, res: Response): Promise<Response> => {
        const {name, email, password} = req.body
        if (!email || !password) return res.json({ msg: 'Por favor agregue un correo y una contraseña.' })
        if (!name) return res.json({ msg: 'Por favor inserte un nombre.' })
        const user = await User.findOne({ email })
        if (user) return res.status(400).json({ msg: 'El correo que intenta ingresar ya existe.' })
        const newUser = new User(req.body)
        await newUser.save()
        return res.json({token: AuthController.createToken(newUser), user: newUser})
    }
    //Crea Token
    public static createToken = (user: IUser): string => sign({ id: user.id, email: user.email }, 'secretkey', { expiresIn: 86400 })
    //Inicia Session
    public static signIn = async (req: Request, res: Response): Promise<Response> => {
        const {email, password} = req.body
        if (!email || !password) {
            return res.status(400).json({ msg: 'Please. Send your email and password' })
        }
        const user = await User.findOne({ email })
        if (!user) return res.status(400).json({ msg: 'The User does not exists' })

        const isMatch = await user.comparePassword(password)
        if (isMatch)  return res.json({ token:  AuthController.createToken(user), user  })
        
        return res.status(400).json({
            msg: 'The email or password are incorrect'
        })
    }
}

export default AuthController