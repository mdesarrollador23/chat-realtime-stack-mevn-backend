import {createServer} from 'http'
import express from 'express'
import { Server } from 'socket.io'
import passport from 'passport'
import passportMiddleware from './middlewares/passport'
// import morgan from 'morgan'
import cors from 'cors'
import moment from 'moment-timezone'
import ChatModels from './models/Chat.models'

// import router
import authRouters from './routes/auth.routes'
import rutaEspec from './routes/ruta.especial'

//inicializacion
const app = express()

//middleware
app.use(cors())
// app.use(morgan('dev'))
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(passport.initialize())
passport.use(passportMiddleware)

//Rutas express
app.use(authRouters)
app.use(rutaEspec)

//socket io
const server = createServer(app)
const options = {
    cors: {
        origin: '*', 
        methods: ['GET","POST'],
        credentials: true
    },
    transport: ['websocket', 'polling']
}
const io = new Server(server,  options)

app.get('/chat', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.json({status: 'OK'})
})

io.on('connection', (socket) => {
    socket.on('newJoin', async (data): Promise<void> => {
        io.emit('join', `${data}, entro al grupo.`)
    })
    socket.on('allChat', async (data): Promise<void> => {
        io.emit('calldata', await ChatModels.find().limit(data))
    })
    socket.on('sendUpdateUsers', async (data): Promise<void> => {
        const date = moment().tz('America/Santo_Domingo').format('h:mm:ss a')
        const msgAll = {
            userID: data._id, name: data.name, msg: data.msg, date 
        }
        const newChat = new ChatModels(msgAll)
        await newChat.save()
        io.emit('chatRealTime', msgAll)
        io.emit('lastmsg', msgAll.msg)
    })
})

export default server
