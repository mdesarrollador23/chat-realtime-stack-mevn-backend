import { Router } from 'express'
const router = Router()
import AuthController from '../controllers/user.controllers'

router.post('/signup', AuthController.signUp)
router.post('/signin', AuthController.signIn)

export default router