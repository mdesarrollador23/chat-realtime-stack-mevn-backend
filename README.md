# hiric - ApiRest Typescript

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run dev
```

### Compiles and minifies for production

```
npm run build
```

### production

```
npm start
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```
